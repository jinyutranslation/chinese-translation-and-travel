# 3 Important Principles for Notary Translation

Notary is a kind of officially assured fraud-deterrent document showing that the target certificate or documentation is authentic. Anyone who has a notary shall be publicly trusted in certain aspects. However, it is working only domestically. According to Chinese law, foreign notarizations have to be translated and re-notarized here before proper use.

![notary translation](https://www.jinyutranslation.com/resources/img/cover_notary.jpg)

Over the past 10 years, we have been working on increasing demands for [notary translation](https://www.jinyutranslation.com/) and have gathered rich experience in it. When translating a notarial document, we are holding our 3 major principles.

---

## Principle 1: Loyal to Original Text
This principle is simple: A translation must be as faithful as possible to its original, otherwise the entire job will be totally ruined. Specifically speaking, when translating a notarization, particular attention shall be addressed on identity confirmation, key details of the notarization, and most importantly, the facts are being certified.

Some of the content to be certified can be identifications, and some are company profiles, while some are assets certifications. We have put great efforts on understanding those files of different uses, in order to make great choices for words conversion, or detail representation.

As a time-honoured service provider, we made large numbers of queries for special terms, and built up our database. Here are some example fields of it:

- Secretary of State of the State(s) of Virginia
- California Corporations Code
- Judicial Sergeant Chief
- Assistant Authentication Officer
- Federal Rules of Civil Procedure
- Notarial Act
- Deputy Clerk

Some of the above terms are seemingly normal, but their Chinese translation needs to be think over.

---

## Principle 2: Double Check Numbers

We know it's a cliche or even fussing to talk about the numbers. But the fact is: there is a great chance that translators make mistake on numerical data, which are often fatal to the following procedures as well as to translators' fame.

Generally, those numbers can be found on:

- Date (the inconsistent expressions can be misleading, e.g. 4-5-2019)
- Address (room or street numbers, postal codes)
- Contact (phone numbers or fax numbers)
- File Numbers
- Identifications (passport numbers, company register numbers)
- Statistic or Computed Numbers (revenue, capital, financing amount)

Thanks to our smart transfer system, most of the numbers can be converted automatically. Meanwhile, we have set redundant inspection procedures to make sure every detail about those numbers is true.

---

## Principle 3: Pay Attention to Typesetting

As for the typesetting or layout of the translation works, it can obviously improve the beauty and thus comprehensive influence. For notarizations, we can easily find scattered stamps, irregular alignments, italic or bold fonts, or mixed figures or tables.

It's not a easy job to reorganize the format according to its original style, but we have gained some experience in it: just follow the font and text align patterns, and make some compromise on figures, signatures or background colors. This is corresponding to industry standard, too.